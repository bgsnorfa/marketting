@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>ADD Consument</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="{{route('project.store')}}" method="post">
					{{csrf_field()}}
						<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
							<label for="nama">Nama</label>
							<input type="text" name="nama" class="form-control" placeholder="Masukan Nama Konsumen" value="{{ old('nama') }}">
							{!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email">Email</label>
							<input type="text" name="email" class="form-control" placeholder="Masukan Email Konsumen" value="{{ old('email') }}">
							{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<label for="phone">Nomor Handphone</label>
							<input type="text" name="phone" class="form-control" placeholder="Nomor Handphone" value="{{ old('phone') }}">
							{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
							<label for="deskripsi">Deskripsi</label>
							<textarea name="deskripsi" class="form-control" placeholder="Deskripsi Pyoyek" >{{ old('deskripsi') }}</textarea>
							{!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Simpan">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
