@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>Pipeline Project</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					@if(Session::has('alert-success'))
					    <div class="alert alert-success">
				            {{ Session::get('alert-success') }}
				        </div>
					@endif

					<div class="btn-group">

						<button type="button" class="btn btn-info">Filter Status</button>
						<button type="button>" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
						</button>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('/admin/project/getindex/'.'aktif')}}">Aktif</a></li>
						<li><a href="{{ url('/admin/project/getindex/'.'non')}}">Non Aktif</a></li>
					  </ul>
					</div>
					<a href="{{route('project.create')}}" class="btn btn-primary pull-right">Tambah Data</a><br><br>

					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Nama Konsumen</th>
							<th style="text-align:center;">Email</th>
							<th style="text-align:center;">No HP</th>
							<th style="text-align:center;">Deskripsi</th>
							<th style="text-align:center;">Status</th>
							<th style="text-align:center;">Action</th>
						</tr>
						<?php $no=1; ?>
						@foreach($projects->sortBy('status') as $projects)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$projects->konsumen}}</td>
							<td>{{$projects->email}}</td>
							<td>{{$projects->hp}}</td>
							<td>{{$projects->deskripsi}}</td>
							<td>{{$projects->status}}</td>
							<td>
								<form method="POST" action="{{ route('project.destroy', $projects->id) }}" accept-charset="UTF-8">
		                            <input name="_method" type="hidden" value="DELETE">
		                            <input name="_token" type="hidden" value="{{ csrf_token() }}">

									<a href="{{route('project.edit', $projects->id)}}" class="btn btn-primary">Edit</a>
		                        	<input type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" value="Delete">

									@if( $projects->status === 'Prospecting')
										<a href="{{url('/admin/project/contact/'.$projects->id)}}" class="btn btn-success">Contact</a>
									@elseif($projects->status === 'Contacted')
										<a href="{{url('/admin/project/contact/'.$projects->id)}}" class="btn btn-info">Discuss</a>
									@endif
		                        </form>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
