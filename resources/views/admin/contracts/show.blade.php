@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>Display specific Contract</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li>
                <i class="fa fa-table"></i> Contract
            </li>
            <li class="active">
                {{ $contract->nama }}
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2 class="well">{{ $contract->nama }}</h2>
        <h4>Quotation number = <i>{{ $contract->id_quo }}</i></h4>
        <br/>
        <a href="{{ url('/admin/contract') }}"><span class="glyphicon glyphicon-arrow-left"></span> Back to Contract list</a>
        <a class="btn btn-primary btn-xs" href="{{ url('/admin/contract/'.$contract->id.'/edit') }}">Edit this contract</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <table class="table">
            <tr>
                <td>Nama Contract</td>
                <td>{{ $contract->nama }}</td>
            </tr>
            <tr>
                <td>Deskripsi</td>
                <td><p>{{ $contract->deskripsi }}</p></td>
            </tr>
            <tr>
                <td>File</td>
                <td><a href="{{ url('/contracts/' . $contract->file) }}" class="btn btn-md btn-info">lihat file</a></td>
            </tr>
            <tr>
                <td>Dibuat</td>
                <td>{{ $contract->created_at }}</td>
            </tr>
            <tr>
                <td>Diperbarui</td>
                <td>{{ $contract->updated_at }}</td>
            </tr>
        </table>
    </div>
</div>

@endsection
