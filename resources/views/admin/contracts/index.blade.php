@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>Contract Overview</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Contract
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="GET" action="{{ url('/admin/contract') }}">
            <div class="form-group col-sm-4 col-md-4">
                <input type="text" name="s" class="form-control" placeholder="Search contract name">
            </div>
            <div class="form-group">
                <button class="btn btn-success">Search</button>
            </div>
        </form>
        @if(isset($message))
            <div class="alert alert-dismissable alert-success fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $message }}
            </div>
        @endif
        <table class="table table-hover table-striped">
            <tr>
                <th>No</th>
                <th>Quotation</th>
                <th>Name</th>
                <th>Description</th>
                <th>Contract file</th>
                <th>Created at</th>
                <th>Action</th>
            </tr>
            <?php $no = 1; ?>
            @foreach($contracts as $item)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->id_quo }}</td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->deskripsi }}</td>
                <td><a class="btn btn-info" href="{{ url('/contracts/' . $item->file) }}" target="_blank">show file</a></td>
                <td>{{ $item->created_at }}</td>
                <td>
                    <a href="{{ url('/admin/contract/'.$item->id) }}" class="btn btn-default btn-xs">Link</a>
                    <a href="{{ url('/admin/contract/' .$item->id . '/edit') }}" class="btn btn-primary btn-xs">Edit</a>
                    <a href="{{ url('/admin/contract/delete/'.$item->id.'?_token='.csrf_token()) }}" onclick="return confirm('Are you sure to delete this data');" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
            @endforeach
        </table>
        {{ $contracts->links() }}
    </div>
</div>

@endsection
