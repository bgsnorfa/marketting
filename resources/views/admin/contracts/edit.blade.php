@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>Edit Contract</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li>
                <i class="fa fa-table"></i> Contract
            </li>
            <li>
                {{ $contract->nama }}
            </li>
            <li class="active">
                <i class="fa fa-pencil-square-o"></i> Edit
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="POST" action="{{ url('/admin/contract/save') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_contract" value="{{ $contract->id }}">
            <input type="hidden" name="quo" value="{{ $contract->id_quo }}">

            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                <label for="input">Nama Contract</label>
                <input type="text" name="nama" placeholder="input contract name" value="{{ $contract->nama }}" class="form-control">
                {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                <label for="input">Deskripsi Contract</label>
                <textarea name="deskripsi" class="form-control" placeholder="input description">{{ $contract->deskripsi }}</textarea>
                {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                <label for="input">Ganti file contract</label>
                <a class="btn btn-primary btn-xs" target="_blank" href="{{ url('/contracts/' . $contract->file) }}"><i class="fa fa-folder-open"></i> Lihat file contract yang sudah ada</a>
                <input type="file" name="file" class="form-control" accept="image/*, .doc, .docx, .xls, .xlsx, .pdf," placeholder="input file">
                {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
            </div>
                <input type="submit" name="save" value="Update" class="btn btn-lg btn-success">
        </form>
    </div>
</div>
@endsection
