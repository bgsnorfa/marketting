@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Invoice <small>Invoice Overview</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li class="active">
                <i class="fa fa-file"></i> Invoice
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<form method="GET" action="{{ url('/admin/invoice') }}">
            <div class="form-group col-sm-4 col-md-4">
                <input type="text" name="s" class="form-control" placeholder="Search contract name">
            </div>
            <div class="form-group">
                <button class="btn btn-success">Search</button>
            </div>
        </form>
		<h3>Choose a contract to see related Invoice(s)</h3>
		<table class="table table-hover table-striped">
			<tr>
				<th>No</th>
				<th>Quotation</th>
				<th>Name</th>
				<th>Description</th>
				<th>Contract file</th>
				<th>See Invoice(s)</th>
			</tr>
			<?php $no = 1; ?>
			@foreach($contracts as $contract)
			<tr>
				<td>{{ $no++ }}</td>
				<td>{{ $contract->id_quo }}</td>
				<td>{{ $contract->nama }}</td>
				<td>{{ $contract->deskripsi }}</td>
				<td><a class="btn btn-info" href="{{ asset('/contracts/' . $contract->file) }}">Show file</a></td>
				<td><a class="btn btn-primary" href="{{ url('/admin/invoice/' . $contract->id_quo) }}">view <i class="fa fa-arrow-right"></i></a></td>
			</tr>
			@endforeach
		</table>
		{{ $contracts->render() }}
	</div>
</div>
@endsection
