@extends('layouts.admin')

@section('content')
<?php
if(isset($_REQUEST['submit'])){
	// $field_values_array = $_REQUEST['field_name'];
	// $field_values_array_2 = $_REQUEST['field_age'];

	print '<pre>';
	print_r(preg_replace('/[\s-]+/', '-', $field_values_array));
	print '</pre>';

	print '<br/><br/>';
	print '<pre>';
	print_r($field_values_array_2);
	print '</pre>';

	// foreach($field_values_array as $value){
	// 	//your database query goes here
	// }
}
?>
<script type="text/javascript">
$(document).ready(function(){
	var maxField = 999; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.field_wrapper'); //Input field wrapper
	var fieldHTML = `<div><input type="text" name="field_name[]" value="" required/><input type="text" name="field_age[]" required><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{ asset('img/remove-icon.png') }}"/></a></div>`; //New input field html
	var x = 1; //Initial field counter is 1
	$(addButton).click(function(){ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
			x++; //Increment field counter
			$(wrapper).append(fieldHTML); // Add field html
		}
	});
	$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent('div').remove(); //Remove field html
		x--; //Decrement field counter
	});
});
</script>
<style type="text/css">
input[type="text"]{height:20px; vertical-align:top;}
.field_wrapper div{ margin-bottom:10px;}
.add_button{ margin-top:10px; margin-left:10px;vertical-align: text-bottom;}
.remove_button{ margin-top:10px; margin-left:10px;vertical-align: text-bottom;}
</style>
</head>
<body>
<form action="" method="post">
<div class="field_wrapper">
	<div>
    	<input type="text" name="field_name[]" value="" required/>
    	<input type="text" name="field_age[]" value="" required/>
        <a href="javascript:void(0);" class="add_button" title="Add field"><img src="{{ asset('img/add-icon.png') }}"/></a>
    </div>
</div>
<input type="hidden" name="_token" value="{{ CSRF_TOKEN() }}">
<input type="submit" name="submit" value="SUBMIT"/>
</form>
</body>
</html>
@endsection
