@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Invoice <small>Invoice for {{ $quot->no }}</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li>
                <i class="fa fa-file"></i> Invoice
            </li>
            <li class="active">
                {{ $quot->no }}
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="well">
            <table class="table">
                <tr>
                    <td>Ship name</td>
                    <td>{{ $quot->konsumen }}</td>
                </tr>
                <tr>
                    <td>Project</td>
                    <td>{{ $quot->proyek }}</td>
                </tr>
                <tr>
                    <td>Ship address</td>
                    <td>{{ $quot->alamat }}</td>
                </tr>
            </table>
        </div>
        <table class="table table-hover table-condensed">
            <tr>
                <th>Invoice no</th>
                <th>Batas akhir</th>
                <th>Item</th>
                <th>Total (+ tax 10%)</th>
                <th>Metode pembayaran</th>
                <th>Action</th>
            </tr>
            @foreach($invoices as $invoice)
            <tr>
                <td>{{ $invoice->no }}</td>
                <td>{{ $invoice->batas_akhir }}</td>
                <td>{!! $invoice->item !!}</td>
                <td>{{ $invoice->total }}</td>
                <td>{{ $invoice->metode }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
