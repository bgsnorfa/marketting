@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Quotation Overview</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li class="active">
                <i class="fa fa-edit"></i> Quotation
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="GET" action="{{ url('/admin/quotation') }}">
            <div class="form-group col-sm-4 col-md-4">
                <input type="text" name="s" class="form-control" placeholder="Search project name">
            </div>
            <div class="form-group">
                <button class="btn btn-success">Search</button>
            </div>
        </form>
        @if(isset($message))
            <div class="alert alert-dismissable alert-success fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $message }}
            </div>
        @endif
        <table class="table table-striped table-hover">
            <tr>
                <th>Quotation no</th>
                <th>Nama</th>
                <th>Proyek</th>
                <th>Admin</th>
                <th>Waktu</th>
                <th>Tanggal pembuatan</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            @foreach($quotations as $item)
            <tr>
                <td>{{ $item->no }}</td>
                <td>{{ $item->konsumen }}</td>
                <td>{{ $item->proyek }}</td>
                <td>{{ $item->admin }}</td>
                <td>{{ $item->waktu }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->status }}</td>
                <td>
                    <a href="{{ url('/admin/quotation/'.$item->id) }}" class="btn btn-default btn-xs">Link</a>
                    <a href="{{ url('/admin/quotation/' .$item->id . '/edit') }}" class="btn btn-primary btn-xs">Edit</a>
                    <a href="{{ url('/admin/quotation/delete/'.$item->id.'?_token='.csrf_token()) }}" onclick="return confirm('Are you sure to delete this data');" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
            @endforeach
        </table>
        {{ $quotations->render() }}
    </div>
</div>
@endsection
