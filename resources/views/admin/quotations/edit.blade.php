@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Edit Quotation</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li>
                <i class="fa fa-edit"></i> Quotation
            </li>
            <li>
                {{ $quotation->proyek }}
            </li>
            <li class="active">
                <i class="fa fa-pencil-square-o"></i> edit
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="POST" action="{{ url('/admin/quotation/save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="quo_id" value="{{ $quotation->id }}">

            @if($quotation->status !== 'accepted')
            <div class="form-group">
                <label for="input">Status</label>
                <select name="status" class="form-control">
                    <option value="">Select status</option>
                    <option value="waiting" selected>Waiting</option>
                    <option value="rejected">Rejected</option>
                </select>
            </div>
            @endif

            <div class="form-group{{ $errors->has('konsumen') ? ' has-error' : '' }}">
                <label for="input">Nama konsumen</label>
                <input type="text" name="konsumen" class="form-control" placeholder="Input name" value="{{ $quotation->konsumen }}">
                {!! $errors->first('konsumen', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('proyek') ? ' has-error' : '' }}">
                <label for="input">Nama Proyek</label>
                <input type="text" name="proyek" class="form-control" placeholder="Input project" value="{{ $quotation->proyek }}">
                {!! $errors->first('proyek', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                <label for="input">Alamat</label>
                <textarea class="form-control" name="alamat" placeholder="Input address">{{ $quotation->alamat }}</textarea>
                {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('term') ? ' has-error' : '' }}">
                <label for="input">Term</label>
                <textarea type="text" name="term" class="form-control" placeholder="input term here">{{ $quotation->term }}</textarea>
                {!! $errors->first('term', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('waktu') ? ' has-error' : '' }}">
                <label for="input">Waktu pengerjaan</label>
                <input type="text" name="waktu" class="form-control" placeholder="Input working time" value="{{ $quotation->waktu }}" >
                {!! $errors->first('waktu', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('spesifikasi') ? ' has-error' : '' }}">
                <label for="input">Spesifikasi</label>
                <textarea type="text" name="spesifikasi" class="form-control" placeholder="input specification here">{{ $quotation->spesifikasi }}</textarea>
                {!! $errors->first('spesifikasi', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
                <label for="input">Harga</label>
                <input type="number" step="1" min="0" name="harga" class="form-control" value="{{ $quotation->harga }}" placeholder="Input price">
                {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
            </div>

            <input type="submit" name="save" class="btn btn-success" value="Update">
        </form>
    </div>
</div>
@endsection
