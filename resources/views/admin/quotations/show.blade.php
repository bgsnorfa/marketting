@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Display specific Quotation</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li>
                <i class="fa fa-edit"></i> Quotation
            </li>
            <li class="active">
                {{ $quotation['proyek'] }}
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2 class="well">{{ $quotation->proyek }}</h2>
        <h4>Quotation number = <i>{{ $quotation->no }}</i></h4>
        <h4>Status = {{ $quotation->status }}</h4>
        <br/>
        <a href="{{ url('/admin/quotation') }}"><span class="glyphicon glyphicon-arrow-left"></span> Back to Quotation</a>
        <a class="btn btn-primary btn-xs" href="{{ url('/admin/quotation/'.$quotation["id"].'/edit') }}">Edit this quotation</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <table class="table">
            <tr>
                <td>Nama Konsumen</td>
                <td>{{ $quotation->konsumen }}</td>
            </tr>

            <tr>
                <td>Alamat Konsumen</td>
                <td>{{$quotation->alamat }}</td>
            </tr>

            <tr>
                <td>Term</td>
                <td><p>{{ $quotation->term }}</p></td>
            </tr>
            <tr>
                <td>Admin</td>
                <td>{{ $quotation->admin }}</td>
            </tr>
            <tr>
                <td>Waktu pengerjaan</td>
                <td>{{ $quotation->waktu }}</td>
            </tr>
            <tr>
                <td>Spesifikasi</td>
                <td><p>{{ $quotation->spesifikasi }}</p></td>
            </tr>
            <tr>
                <td>Harga</td>
                <td>IDR {{ $quotation->harga }}</td>
            </tr>
            <tr>
                <td>On Contract</td>
                <td>{{ $quotation->oncontract }}</td>
            </tr>
            <tr>
                <td>Dibuat</td>
                <td>{{ $quotation->created_at }}</td>
            </tr>
            <tr>
                <td>Diperbarui</td>
                <td>{{ $quotation->updated_at }}</td>
            </tr>
        </table>
    </div>
</div>

@endsection
