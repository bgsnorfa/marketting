<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invoices;
use App\Contracts;
use App\Quotations;
use Redirect;
use DB;

class InvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('s')){
            $contracts = Contracts::where('nama', 'LIKE', '%' . $request->s . '%')->paginate(10)->appends(['s' => $request->s]);
        } else {
            $contracts = Contracts::paginate(10);
        }
        return view('admin.invoices.index')->with('title', 'Invoice')->with('contracts', $contracts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->has('submit')){
            $field_values_array = $request->field_name;
        	$field_values_array_2 = $request->field_age;
            // $field_values_array = $_REQUEST['field_name'];
        	// $field_values_array_2 = $_REQUEST['field_age'];
            return view('admin.invoices.add')->with('field_values_array', $field_values_array)->with('field_values_array_2', $field_values_array_2);
        } else {
            return view('admin.invoices.add');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quo = $id;
        //get quotation
        $quot = Quotations::where('no',$quo)->first();
        //get invoices
        $invoices = Invoices::where('quo', $quo)->paginate(5);
        return view('admin.invoices.show')->with('title', 'Invoices detail')->with('invoices', $invoices)->with('quot', $quot);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
