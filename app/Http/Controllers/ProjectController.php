<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Requests;
use App\Http\Requests\Project\StoreRequest;
use App\Http\Requests\Project\UpdateRequest;
use DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
		$projects = new Project();
		$projects->konsumen=$request->nama;
		$projects->email=$request->email;
		$projects->hp=$request->phone;
		$projects->deskripsi=$request->deskripsi;
		$projects->status="Prospecting";
        $projects->save();
        return redirect()->route('project.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$projects = Project::findOrFail($id)->first();
        $projects = Project::find($id);
        return view('admin.projects.edit', compact('projects'));
		//return view('edit')->with('projects',$projects);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $projects = Project::findOrFail($id);
		$projects->konsumen=$request->nama;
		$projects->email=$request->email;
		$projects->hp=$request->phone;
		$projects->deskripsi=$request->deskripsi;
        $projects->save();
        return redirect()->route('project.index')->with('alert-success', 'Data Berhasil Diubah.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects = Project::findOrFail($id);
        $projects->delete();
        return redirect()->route('project.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }

	public function contact($id)
	{
		$projects = Project::findOrFail($id);
		$projects->status="Contacted";
        $projects->save();
        return redirect()->route('project.index')->with('alert-success', 'Pelanggan Telah Berhasil Dihubungi.');
	}

	public function getIndex($get){
	  $projects = DB::table('projects');
	  if($get=='aktif'){
		$projects->where('projects.status','Prospecting');
	  }
	  if($get=='non'){
		$projects->where('projects.status','Contacted');
	  }
	  $projects = $projects->paginate(10);
	  return view('projects.index',['projects' => $projects]);
	}

}
