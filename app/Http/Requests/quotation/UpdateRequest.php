<?php

namespace App\Http\Requests\quotation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Rule::unique('users')->ignore($user->id),
            'konsumen' => 'Required',
            'proyek' => 'Required',
            'alamat' => 'Required',
            'term' => 'Required',
            'waktu'=> 'Required',
            'spesifikasi' => 'Required',
            'harga' => 'Required|integer'
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }
}
