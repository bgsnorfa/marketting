<?php

namespace App\Http\Requests\contract;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'Required',
            'deskripsi' => 'Required',
            'file' => 'Required|file|mimes:pdf,xls,xlsx,doc,docx'
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
