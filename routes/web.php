<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');

Route::group(['middleware'=>['auth']], function (){
    Route::get('/admin', function (){
        return view('admin.index');
    });

    /*projects controller
    consist of
    create
    retrieve
    update
    Delete
    */
    Route::resource('project', 'ProjectController');
    Route::get('/admin/project', 'ProjectController@index');
    Route::get('/admin/project/contact/{id}', 'ProjectController@contact');
    Route::get('/admin/project/getindex/{get}', 'ProjectController@getindex');

    /*quotation controller
    consist of
    create
    Retrieve
    update
    Delete
    */
    //create form for quotation
    Route::get('/admin/quotation/add', 'QuotationsController@add');
    //store quotation data
    Route::post('/admin/quotation/store', 'QuotationsController@store');
    //retrieve quotations
    Route::get('/admin/quotation', 'QuotationsController@index');
    //retrieve specific quotation
    Route::get('/admin/quotation/{id}', 'QuotationsController@show');
    //edit form for specific quotation
    Route::get('/admin/quotation/{id}/edit', 'QuotationsController@edit');
    //update quotation
    Route::post('/admin/quotation/save', 'QuotationsController@update');
    //delete quotation
    Route::get('/admin/quotation/delete/{id}', 'QuotationsController@destroy');

    /*Contract controller
    consist of
    retrieve
    create
    update
    Delete
    */
    //retrieve contracts
    Route::get('/admin/contract', 'ContractsController@index');
    //display add contract form
    Route::get('/admin/contract/add', 'ContractsController@create');
    //store contract data
    Route::post('/admin/contract/store', 'ContractsController@store');
    //delete contract data
    Route::get('/admin/contract/delete/{id}', 'ContractsController@destroy');
    //retrieve specific contract
    Route::get('/admin/contract/{id}', 'ContractsController@show');
    //edit form for specific contract
    Route::get('/admin/contract/{id}/edit', 'ContractsController@edit');
    //update contract
    Route::post('/admin/contract/save', 'ContractsController@update');

    /*Invoice Controller
    consist of
    retrieve
    create
    update
    Delete
    */
    //retrieve Invoice
    Route::get('/admin/invoice', 'InvoicesController@index');
    //display invoices add form
    Route::get('/admin/invoice/add', 'InvoicesController@create');
    //display invoices based on quotation number
    Route::get('/admin/invoice/{id}', 'InvoicesController@show');
    // Route::post('/admin/invoice', 'InvoicesController@index');
});
