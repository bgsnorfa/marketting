<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function(Blueprint $table){
            $table->increments('id');
            $table->string('no')->default('QUO');
            $table->string('konsumen');
            $table->string('proyek');
            $table->text('term');
            $table->text('alamat');
            $table->string('admin')->references('name')->on('users')->onDelete('cascade');
            $table->string('waktu');
            $table->text('spesifikasi');
            $table->string('harga');
            $table->enum('status', ['accepted', 'rejected', 'waiting'])->default('waiting');
            $table->enum('oncontract', ['yes', 'no'])->default('no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotations');
    }
}
